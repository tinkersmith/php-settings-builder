<?php

/*
 * This file is part of the PHP Settings Builder package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tinkersmith\SettingsBuilder\Php\Expr;

/**
 * The base interface for PHP expressions which represent a value assignment.
 */
interface AssignmentInterface extends ExpressionInterface
{

    /**
     * @return string Get the left hand value of the assignment (variable name).
     */
    public function getVariableName(): string;

    /**
     * @param string  $varName The base variable name for the value to assign.
     * @param mixed[] $keys    The array keys if variable name is an array, and the target value is nested in the array.
     *
     * @return self
     */
    public function setVariableName(string $varName, array $keys = null): self;
}
