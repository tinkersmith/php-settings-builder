<?php

/*
 * This file is part of the PHP Settings Builder package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tinkersmith\SettingsBuilder\Php\Expr;

use Tinkersmith\SettingsBuilder\Php\Dumper;

/**
 * Class for representing a PHP native type to be written out to a PHP file.
 *
 * This class supports transforming a PHP native type into a string value for
 * output to a PHP file. Should support integer, float, booleans, strings and
 * arrays.
 *
 * The string output is formatted using the Dumper::dump() method.
 *
 * @see \Tinkersmith\SettingsBuilder\Php\Dumper::dump()
 */
class PhpValue implements ExpressionInterface
{

    /**
     * @param mixed $value The PHP primative value to represent with this expression.
     */
    public function __construct(protected mixed $value)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getValue(): mixed
    {
        return $this->value;
    }

    /**
     * {@inheritdoc}
     */
    public function dump(Dumper $dumper, string $indent = ''): string
    {
        return $dumper($this->value, $indent);
    }
}
