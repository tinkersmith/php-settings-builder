<?php

/*
 * This file is part of the PHP Settings Builder package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tinkersmith\SettingsBuilder\Php\Expr;

use Tinkersmith\SettingsBuilder\Php\Dumper;
use Tinkersmith\SettingsBuilder\Php\NamespacedInterface;

/**
 * The base class for PHP expressions which represent a value assignment.
 */
class Assignment implements AssignmentInterface, NamespacedInterface
{

    /**
     * @param string $varName The left hand side of the assignment statement.
     * @param mixed  $value   The value to assign. Can be an expression object or a PHP value.
     */
    public function __construct(protected string $varName, protected mixed $value)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getNamespaces(): array
    {
        if ($this->value instanceof NamespacedInterface) {
            return $this->value->getNamespaces();
        }

        $namespaces = [];
        if (is_array($this->value)) {
            foreach ($this->value as $val) {
                if ($val instanceof NamespacedInterface) {
                    $namespaces = array_merge($namespaces, $val->getNamespaces());
                }
            }
        }

        return $namespaces;
    }

    /**
     * {@inheritdoc}
     */
    public function getVariableName(): string
    {
        return $this->varName;
    }

    /**
     * {@inheritdoc}
     */
    public function setVariableName(string $varName, array $keys = null): self
    {
        $this->varName = $varName;

        if ($keys) {
            $this->varName .= '['.implode('][', $keys).']';
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function dump(Dumper $dumper, string $indent = ''): string
    {
        return $this->varName.' = '.$dumper($this->value, $indent);
    }
}
