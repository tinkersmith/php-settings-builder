<?php

/*
 * This file is part of the PHP Settings Builder package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tinkersmith\SettingsBuilder\Php\Expr;

use Tinkersmith\SettingsBuilder\Php\Dumper;

/**
 * Class for representing a simple PHP expression.
 *
 * The value for this class is a string, and is written as is to the output as
 * is. This allows expressions which are function calls, variables, or
 * a valid PHP expression (which evaluate to a value).
 *
 * If you are looking to write out a PHP value, or array use PhpValueAssignment
 * class instead. This will format the PHP value into the correct string output
 * for a PHP file.
 */
class Expression implements ExpressionInterface
{

    /**
     * @param string $expr Value to write for the assignment. Override to allow mixed value types.
     */
    public function __construct(protected string $expr)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function dump(Dumper $dumper, string $indent = ''): string
    {
        return $this->expr;
    }
}
