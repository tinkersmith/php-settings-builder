<?php

/*
 * This file is part of the PHP Settings Builder package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tinkersmith\SettingsBuilder\Php\Expr;

use Tinkersmith\SettingsBuilder\Php\Dumper;

/**
 * Base interface for representing a PHP expression that is writable to a file.
 */
interface ExpressionInterface
{

    /**
     * @param Dumper $dumper The PHP code formatter and dumper object.
     * @param string $indent The current indent level of the expression. Expressions should only apply these for
     *                       nested or inner statements.
     *
     * @return string The styled string representation of the expression.
     */
    public function dump(Dumper $dumper, string $indent = ''): string;
}
