<?php

/*
 * This file is part of the PHP Settings Builder package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tinkersmith\SettingsBuilder\Php\Expr;

use Tinkersmith\SettingsBuilder\Php\Dumper;
use Tinkersmith\SettingsBuilder\Php\NamespacedInterface;

/**
 * Expression class represents the instantiation of a new class instance.
 */
class InstantiateExpression extends InvokeExpression implements ExpressionInterface, NamespacedInterface
{

    /**
     * @param string  $classname Fully namespaced class name.
     * @param mixed[] $args      Contstructor arguments using the same conventions as InvokeExpression (parent class).
     *
     * @see InvokeExpression::formatArguments()
     */
    public function __construct(protected string $classname, protected array $args = [])
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getNamespaces(): array
    {
        return [$this->classname];
    }

    /**
     * {@inheritdoc}
     */
    public function dump(Dumper $dumper, string $indent = ''): string
    {
        // Get the class name to use in this context.
        $name = $dumper
            ->getContextNamespaces()
            ->getName($this->classname, false);

        return "new {$name}(".$this->formatArguments($dumper, $indent).')';
    }
}
