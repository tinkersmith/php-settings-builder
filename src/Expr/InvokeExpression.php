<?php

/*
 * This file is part of the PHP Settings Builder package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tinkersmith\SettingsBuilder\Php\Expr;

use Tinkersmith\SettingsBuilder\Php\Dumper;
use Tinkersmith\SettingsBuilder\Php\NamespacedInterface;

/**
 * Expression that respresents and the invocation of a callable.
 */
class InvokeExpression implements ExpressionInterface, NamespacedInterface
{

    /**
     * This instance is invoking a static method.
     *
     * @var bool
     */
    protected bool $isStatic = false;

    /**
     * The fully namespaced class name for static method invocations.
     *
     * Note that "self" and "static" are both ways to call static methods but
     * without having a namespace, but these should only be used inside a class
     * definition context.
     *
     * @var string|null
     */
    protected ?string $namespace = null;

    /**
     * The string representing the object or class of a method invocation.
     *
     * This value is null when $this->func is not a method and just a function
     * at the top level scope.
     *
     * @var string|null
     */
    protected ?string $object = null;

    /**
     * String expression or callable name.
     *
     * @var string
     */
    protected string $func;

    /**
     * @param string  $func The callable to invoke with this method.
     * @param mixed[] $args Arguments to pass to the callable. See ::formatArguments().
     */
    public function __construct(string $func, protected array $args = [])
    {
        if (preg_match('/^([\\a-z0-9_]+)(::|->)(.*)$/i', $func, $matches)) {
            if ('::' === $matches[2]) {
                $this->isStatic = true;

                if (!in_array($matches[1], ['self', 'static'])) {
                    $this->namespace = $matches[1];
                }
            }
            $this->object = $matches[1];
            $this->func = $matches[3];
        } else {
            $this->func = $func;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getNamespaces(): array
    {
        return $this->namespace ? [$this->namespace] : [];
    }

    /**
     * {@inheritdoc}
     */
    public function dump(Dumper $dumper, string $indent = ''): string
    {
        $invoke = $this->func;
        if ($this->object) {
            $namespaces = $dumper->getContextNamespaces();
            $object = $this->isStatic
                ? ($this->namespace ? $namespaces->getName($this->object) : $this->object).'::'
                : $this->object.'->';

            $invoke = $object.$invoke;
        }

        return $invoke.'('.$this->formatArguments($dumper, $indent).')';
    }

    /**
     * Format the callable's arguments into a string format for settings output.
     *
     * Arguments can be provided as and array with numerical keys or with
     * string keys. When keys are string, they are treated as named arguments
     * and output with <code>"key": "value"</code> format per the PHP 8.0
     * specification.
     *
     * @param Dumper $dumper The PHP statement or expression output dumper. Ensures language constructs are written out
     *                       in a string output format for PHP settings files.
     * @param string $indent The current indent of the callable, so parameters that wrap can indent correctly.
     *
     * @return string Arguments for the callable being invoked. Nested expressions are resolved to strings.
     */
    protected function formatArguments(Dumper $dumper, string $indent = ''): string
    {
        $formatted = [];
        foreach ($this->args as $key => $arg) {
            $formatted[] = (is_numeric($key) ? '':"$key: ").$dumper($arg, $indent);
        }

        return implode(', ', $formatted);
    }
}
