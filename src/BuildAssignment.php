<?php

/*
 * This file is part of the PHP Settings Buider package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tinkersmith\SettingsBuilder\Php;

use Tinkersmith\SettingsBuilder\Php\Expr\AssignmentInterface;
use Tinkersmith\SettingsBuilder\Php\Stmt\StatementInterface;

/**
 * Class for SettingsBuilder to wrap an assignment that can be written.
 */
class BuildAssignment extends BuildItem
{

    /**
     * @param AssignmentInterface $item  The assignment to build.
     * @param bool                $flush Should the settings builder flush previous items when this item is written.
     */
    public function __construct(public StatementInterface|AssignmentInterface $item, public bool $flush = false)
    {
        if (!$item instanceof AssignmentInterface) {
            throw new \InvalidArgumentException('The build item for the BuildAssignment class must be a Assignment type.');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function dump(SettingsBuilder $builder, Dumper $dumper, string $indent = ''): ?string
    {
        $this->isWritten = true;

        return preg_replace("/(^|\r?\n|\r)/", "\$1{$indent}", $dumper($this->item)).';';
    }
}
