<?php

/*
 * This file is part of the PHP Settings Builder package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tinkersmith\SettingsBuilder\Php;

use Tinkersmith\SettingsBuilder\Php\Expr\AssignmentInterface;
use Tinkersmith\SettingsBuilder\Php\Expr\ExpressionInterface;
use Tinkersmith\SettingsBuilder\Php\Stmt\StatementInterface;

/**
 * Class to transform values into a string compatible for use in a PHP file.
 *
 * Class supports PHP primatives and object that implement the Raini
 * ExpressionInterface interface.
 *
 * Use a PhpExpression object if you wish to generate expressions that printed
 * as is.
 */
final class Dumper
{

    /**
     * The current declared namespaces to references and "use" declarations.
     *
     * @var Namespaces|null
     */
    public ?Namespaces $namespaces;

    /**
     * @param PhpStyle $style The PHP styles settings for the dumper's formatted output.
     */
    public function __construct(public PhpStyle $style = new PhpStyle())
    {
    }

    /**
     * Get the current namespaces associated with this Dumper context.
     *
     * @return Namespaces Get the current namespaces context.
     */
    public function getContextNamespaces(): Namespaces
    {
        if (!isset($this->namespaces)) {
            $this->namespaces = new Namespaces();
        }

        return $this->namespaces;
    }

    /**
     * @param string $value  The PHP native string value to output.
     * @param string $indent The starting indent for the output value.
     *
     * @return string The text representation of the string $value. This will properly escape characters and ensure the
     *                text output is properly quoted.
     */
    public function dumpString(string $value, string $indent = ''): string
    {
        if (!$value) {
            return "''";
        }

        $subIndent = $indent.$this->style->indent;
        $out = sprintf("'%s'", addcslashes($value, "'\\"));
        $out = preg_replace_callback("/((?:[\\0\\r\\n]|\u{202A}|\u{202B}|\u{202D}|\u{202E}|\u{2066}|\u{2067}|\u{2068}|\u{202C}|\u{2069})++)(.)/", function ($m) use ($subIndent) {
            $m[1] = sprintf('\'."%s".\'', str_replace(
                ["\0", "\r", "\n", "\u{202A}", "\u{202B}", "\u{202D}", "\u{202E}", "\u{2066}", "\u{2067}", "\u{2068}", "\u{202C}", "\u{2069}", '\n\\'],
                ['\0', '\r', '\n', '\u{202A}', '\u{202B}', '\u{202D}', '\u{202E}', '\u{2066}', '\u{2067}', '\u{2068}', '\u{202C}', '\u{2069}', '\n"'."\n".$subIndent.'."\\'],
                $m[1]
            ));

            if ("'" === $m[2]) {
                return substr($m[1], 0, -2);
            }

            if (str_ends_with($m[1], 'n".\'')) {
                return substr_replace($m[1], "\n".$subIndent.".'".$m[2], -2);
            }

            return $m[1].$m[2];
        }, $out, -1, $count);

        return $count && str_starts_with($out, "''.") ? substr($out, 3) : $out;
    }

    /**
     * @param mixed[] $value  The array to transform into a string representation.
     * @param string  $indent The starting indent for the output value.
     *
     * @return string A text representation of the $value array.
     */
    public function dumpArray(array $value, string $indent = ''): string
    {
        if (!$value) {
            return '[]';
        }

        $prev = -1;
        $out = "[\n";
        $subIndent = $indent.$this->style->indent;

        foreach ($value as $key => $val) {
            $out .= $subIndent;

            if (!\is_int($key) || 1 !== $key - $prev) {
                $out .= $this($key).' => ';
            }
            if (\is_int($key) && $key > $prev) {
                $prev = $key;
            }

            $out .= $this($val, $subIndent).",\n";
        }

        return $out.$indent.']';
    }

    /**
     * @param mixed  $value  Value to transform into a text represenation for use in a PHP file.
     * @param string $indent The starting indent for the output value.
     *
     * @return string The text representation for the value provided.
     *
     * @throws \UnexpectedValueException Thrown when value cannot be translate to a text represenation. Usually this
     *                                   will be from objects that the dumper is unsure how to transform.
     */
    public function __invoke(mixed $value, string $indent = ''): string
    {
        // Find the first true case statement.
        switch (true) {
            case \is_int($value) || \is_float($value):
                return var_export($value, true);
            case false === $value:
                return $this->style->capsBool ? 'FALSE' : 'false';
            case true === $value:
                return  $this->style->capsBool ? 'TRUE' : 'true';
            case null === $value:
                return $this->style->capsNull ? 'NULL' : 'null';
            case $value instanceof \UnitEnum:
                return ltrim(var_export($value, true), '\\');
            case \is_string($value):
                return $this->dumpString($value, $indent);
            case \is_array($value):
                return $this->dumpArray($value, $indent);
            case $value instanceof StatementInterface:
            case $value instanceof ExpressionInterface:
                return $value->dump($this, $indent);
        }

        throw new \UnexpectedValueException(sprintf('Cannot export value of type "%s".', get_debug_type($value)));
    }

    /**
     * Get the indent for the dumper's PHP style settings.
     *
     * @return string The current indent string value based on the set PHP style.
     */
    public function indent(): string
    {
        return $this->style->indent;
    }
}
