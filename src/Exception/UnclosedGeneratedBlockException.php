<?php

/*
 * This file is part of the PHP Settings builder package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tinkersmith\SettingsBuilder\Php\Exception;

/**
 * Exception thrown when the SettingsBuilder is unable to match code block tags.
 *
 * The SettingsBuilder tries to match opening and closing code block markers to
 * determine where generated code sections start and end. This exception is to
 * indicate that a starting marker was found but no matching ending marker could
 * be found.
 *
 * This makes it difficult to tell if there are parts of the source that are
 * missing or unreadable. Instead of writing a potentially incomplete setting
 * this error is thrown instead.
 */
class UnclosedGeneratedBlockException extends \Exception
{

    /**
     * @param string          $blockId The generated code block identifier.
     * @param \Throwable|null $prev    The previous exception or throwable error when chaining errors.
     */
    public function __construct(string $blockId, \Throwable $prev = null)
    {
        $message = 'Unable to find the end of the generated code block for: '.$blockId;

        parent::__construct($message, 0, $prev);
    }
}
