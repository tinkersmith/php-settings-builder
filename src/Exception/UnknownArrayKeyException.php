<?php

/*
 * This file is part of the PHP Settings builder package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tinkersmith\SettingsBuilder\Php\Exception;

/**
 * Exception for when a PHP parsing is unable to determine an array key value.
 *
 * The array key maybe valid, it just can't be determined by the parser either
 * because it is dynamic, is a variable, or an expression.
 */
class UnknownArrayKeyException extends \Exception
{
}
