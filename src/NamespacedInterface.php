<?php

/*
 * This file is part of the PHP Settings Builder package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tinkersmith\SettingsBuilder\Php;

/**
 * Interface for SettingsBuilder build elements that define namespaces.
 *
 * Classes that implement this interface may define class namespaces and use
 * this interface to report that back to the SettingsBuilder.
 */
interface NamespacedInterface
{

    /**
     * A list of class namespaces used by this settings build element.
     *
     * @return string[] The lists of fully namespaced classes used by this expression or statement.
     */
    public function getNamespaces(): array;
}
