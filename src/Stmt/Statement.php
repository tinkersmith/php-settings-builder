<?php

/*
 * This file is part of the PHP Settings Builder package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tinkersmith\SettingsBuilder\Php\Stmt;

use Tinkersmith\SettingsBuilder\Php\Dumper;
use Tinkersmith\SettingsBuilder\Php\Expr\Expression;
use Tinkersmith\SettingsBuilder\Php\Expr\ExpressionInterface;
use Tinkersmith\SettingsBuilder\Php\NamespacedInterface;

/**
 * A basic simple statement.
 *
 * The minimal statement, which consist primarily of a simple expression. The
 * main difference is that the statement is complete in and of itself, while an
 * expression can be code that can be part of a bigger construct.
 */
class Statement extends AbstractStatement implements StatementInterface
{

    /**
     * A simple expression that is the statement body.
     *
     * @var ExpressionInterface
     */
    protected ExpressionInterface $value;

    /**
     * @param string|ExpressionInterface $statement The statement expression or string.
     * @param string|null                $comments  The statement comments if there are any.
     */
    public function __construct(string|ExpressionInterface $statement, public ?string $comments = null)
    {
        $this->value = is_string($statement)
            ? new Expression($statement)
            : $statement;
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty(): bool
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function getNamespaces(): array
    {
        return $this->value instanceof NamespacedInterface
            ? $this->value->getNamespaces()
            : [];
    }

    /**
     * {@inheritdoc}
     */
    public function dump(Dumper $dumper, string $indent = ''): string
    {
        $output = $this->dumpComments($indent);
        $output .= "\n{$indent}".$dumper($this->value, $indent);

        // Statements from expressions should end with semicolons.
        if (!str_ends_with($output, ';')) {
            $output .= ';';
        }

        return $output;
    }
}
