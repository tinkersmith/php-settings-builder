<?php

/*
 * This file is part of the PHP Settings Builder package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tinkersmith\SettingsBuilder\Php\Stmt;

use Tinkersmith\SettingsBuilder\Php\Dumper;

/**
 * A group or block of multiple statements to be output together.
 */
class StatementsBlock extends AbstractStatement implements StatementInterface
{

    /**
     * Statements that belong to this block.
     *
     * @var StatementInterface[]
     */
    public array $statements;

    /**
     * @param StatementInterface|StatementInterface[] $statements Either a single statement or an array of statements to
     *                                                            execute when the condition express evaluates to true.
     * @param string|null                             $comments   Comments for this statement.
     */
    public function __construct(array|StatementInterface $statements, public ?string $comments = null)
    {
        $this->statements = is_array($statements) ? $statements : [$statements];
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty(): bool
    {
        return empty($this->statements);
    }

    /**
     * {@inheritdoc}
     */
    public function getNamespaces(): array
    {
        $namespaces = [];
        foreach ($this->statements as $stmt) {
            if ($tmp = $stmt->getNamespaces()) {
                $namespaces = array_merge($namespaces, $tmp);
            }
        }

        return $namespaces;
    }

    /**
     * {@inheritdoc}
     */
    public function dump(Dumper $dumper, string $indent = ''): string
    {
        return $this->dumpStatements($this->statements, $dumper, $indent);
    }

    /**
     * Dump all the statements in this block into a string value.
     *
     * @param StatementInterface[] $statements The list of statements in this block to write.
     * @param Dumper               $dumper     The PHP dumper that converts and formats the statements to PHP strings.
     * @param string               $indent     The current indent to apply the start of each statement.
     *
     * @return string The formatted statements output string.
     */
    protected function dumpStatements(array $statements, Dumper $dumper, string $indent = ''): string
    {
        $output = '';
        foreach ($statements as $statement) {
            $output .= $statement->dump($dumper, $indent);
        }

        return rtrim($output);
    }
}
