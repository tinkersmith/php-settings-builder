<?php

/*
 * This file is part of the PHP Settings Builder package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tinkersmith\SettingsBuilder\Php\Stmt;

use Tinkersmith\SettingsBuilder\Php\Dumper;

/**
 * A group of multiple conditional statements rendered together.
 */
class ConditionalGroup extends StatementsBlock implements StatementInterface
{
    /**
     * The conditional group join value.
     *
     * The first conditional of the group always is an "if" condition, but
     * the remaining conditionals can be "if" or "elseif" which is the value
     * set here.
     *
     * @var string
     */
    protected string $join = 'elseif';

    /**
     * @param string        $join       The conditional join type value.
     * @param Conditional[] $conditions The list of conditional statements that belong to this group.
     */
    public function __construct(string $join = 'elseif', protected array $conditions = [])
    {
        $this->setJoin($join);
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty(): bool
    {
        return empty($this->conditions);
    }

    /**
     * {@inheritdoc}
     */
    public function getNamespaces(): array
    {
        $namespaces = [];
        foreach ($this->conditions as $cond) {
            $namespaces = array_merge($namespaces, $cond->getNamespaces());
        }

        return $namespaces;
    }

    /**
     * Get the current conditional group join value.
     *
     * This value should be either "if" or "elseif" and is the condition type
     * after the initial one (first is always "if").
     *
     * @return string Get the current condition join value.
     */
    public function getJoin(): string
    {
        return $this->join;
    }

    /**
     * Set the conditional group join value.
     *
     * The first conditional of the group always is an "if" condition, but
     * the remaining conditionals can be "if" or "elseif" which is the value
     * set here.
     *
     * @param string $join The conditional join value to set.
     */
    public function setJoin(string $join): void
    {
        if (!in_array($join, ['if', 'elseif'])) {
            throw new \InvalidArgumentException('Conditional groups only support "if" or "elseif" for conditional statements.');
        }

        $this->join = $join;
    }

    /**
     * Adds a condition statement to the group.
     *
     * @param Conditional $cond Condition statement to add to the group.
     */
    public function addCondition(Conditional $cond): void
    {
        $this->conditions[] = $cond;
    }

    /**
     * {@inheritdoc}
     */
    public function dump(Dumper $dumper, string $indent = ''): string
    {
        $output = '';
        if ($cond = reset($this->conditions)) {
            $output = $this->dumpCondition('if', $cond, $dumper, $indent);

            while ($cond = next($this->conditions)) {
                $output .= $this->dumpCondition($this->join, $cond, $dumper, $indent);
            }
            $output .= "\n";
        }

        return $output;
    }

    /**
     * Generate the code output for a conditional statement.
     *
     * @param String      $join   The conditional type to output ()"if" or "elseif").
     * @param Conditional $cond   The conditional statement to output.
     * @param Dumper      $dumper The code dumper object.
     * @param string      $indent The current indent to apply to statements.
     *
     * @return string The condition formatted string value.
     */
    protected function dumpCondition(string $join, Conditional $cond, Dumper $dumper, string $indent = ''): string
    {
        $output = $cond->dumpComments($indent);
        $output .= "\n{$indent}{$join} (".$dumper($cond->getCondition()).') {';
        $output .= $this->dumpStatements($cond->statements, $dumper, $indent.$dumper->indent());

        return $output."\n{$indent}}";
    }
}
