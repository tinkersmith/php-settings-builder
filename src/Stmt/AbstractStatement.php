<?php

/*
 * This file is part of the PHP Settings Builder package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tinkersmith\SettingsBuilder\Php\Stmt;

/**
 * The base statement class to ensure consistent handling of comments.
 */
abstract class AbstractStatement implements StatementInterface
{
    /**
     * The statement's comments if any.
     *
     * These are comments printed directly before the statement, and are
     * literally printed as is with the lines indented properly. All forms of
     * valid PHP comments should be allowed, but the string must contain the
     * comment characters ('#', '//' or '/*').
     *
     * @var string|null
     */
    public ?string $comments;

    /**
     * {@inheritdoc}
     */
    public function setComments(string $comments = null): void
    {
        $this->comments = $comments;
    }

    /**
     * {@inheritdoc}
     */
    public function dumpComments(string $indent = ''): string
    {
        if (!$this->comments) {
            return '';
        }

        // Apply the proper indenting to comments.
        return "\n{$indent}".preg_replace("/(\n|\r\n?)/m", "\$1{$indent}", $this->comments);
    }
}
