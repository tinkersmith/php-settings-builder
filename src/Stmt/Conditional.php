<?php

/*
 * This file is part of the PHP Settings Builder package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tinkersmith\SettingsBuilder\Php\Stmt;

use Tinkersmith\SettingsBuilder\Php\Dumper;
use Tinkersmith\SettingsBuilder\Php\Expr\Expression;
use Tinkersmith\SettingsBuilder\Php\Expr\ExpressionInterface;
use Tinkersmith\SettingsBuilder\Php\NamespacedInterface;

/**
 * Class for representing a conditional "if" statement.
 *
 * The statement consists of a truthy expression to evaluate for the conditional
 * test and the statements to execute if the expression evalutes to true.
 */
class Conditional extends StatementsBlock implements StatementInterface
{

    /**
     * The condition expression of the conditional block.
     *
     * This is the truthy expression of the condtion, which determines if the
     * statements are executed or not.
     *
     * @var ExpressionInterface
     */
    protected ExpressionInterface $condition;

    /**
     * @param string|ExpressionInterface              $condition  The conditional expression to evaluate.
     * @param StatementInterface|StatementInterface[] $statements Either a single statement or an array of statements to
     *                                                            execute when the condition express evaluates to true.
     * @param string|null                             $comments   Comments for this statement.
     */
    public function __construct(string|ExpressionInterface $condition, array|StatementInterface $statements, public ?string $comments = null)
    {
        parent::__construct($statements, $comments);

        $this->setCondition($condition);
    }

    /**
     * {@inheritdoc}
     */
    public function getNamespaces(): array
    {
        $namespaces = parent::getNamespaces();
        if ($this->condition instanceof NamespacedInterface) {
            $namespaces = array_merge($namespaces, $this->condition->getNamespaces());
        }

        return $namespaces;
    }

    /**
     * Gets the condition expression to evaluate.
     *
     * @return ExpressionInterface The "if" condition expression.
     */
    public function getCondition(): ExpressionInterface
    {
        return $this->condition;
    }

    /**
     * Sets the conditional expression for the "if" statement.
     *
     * If the value is a string it is converted into an expression to ensure
     * the value is handled consistently and rendered correctly.
     *
     * @param string|ExpressionInterface $condition The conditional truthy expression for the "if" statement.
     */
    public function setCondition(string|ExpressionInterface $condition): void
    {
        $this->condition = $condition instanceof ExpressionInterface
            ? $condition : new Expression($condition);
    }

    /**
     * {@inheritdoc}
     */
    public function dump(Dumper $dumper, string $indent = ''): string
    {
        if (!$this->statements) {
            return '';
        }

        $output = $this->dumpComments($indent);
        $output .= "\n{$indent}if (".$dumper($this->condition).') {';
        $output .= $this->dumpStatements($this->statements, $dumper, $indent.$dumper->indent());

        return $output."\n{$indent}}\n";
    }
}
