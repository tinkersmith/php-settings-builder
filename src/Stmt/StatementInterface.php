<?php

/*
 * This file is part of the PHP Settings Builder package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tinkersmith\SettingsBuilder\Php\Stmt;

use Tinkersmith\SettingsBuilder\Php\Dumper;
use Tinkersmith\SettingsBuilder\Php\NamespacedInterface;

/**
 * Interface for classes that represent PHP code statements.
 */
interface StatementInterface extends NamespacedInterface
{

    /**
     * @return bool Is this statement empty? TRUE iff it is, and FALSE otherwise.
     */
    public function isEmpty(): bool;

    /**
     * Sets the comments that appear above the statement.
     *
     * @param string $comments The comments to set for this statement.
     */
    public function setComments(string $comments = null): void;

    /**
     * Formats the comments into string output.
     *
     * @param string $indent The indent to apply to the start of a comment line of text.
     *
     * @see AbstractStatement::dumpComents()
     */
    public function dumpComments(string $indent = ''): string;

    /**
     * Converts the statement into a formatted code string.
     *
     * @param Dumper $dumper The PHP dumper object (converts values to strings).
     * @param string $indent The indent to apply to the start of statements.
     */
    public function dump(Dumper $dumper, string $indent = ''): string;
}
