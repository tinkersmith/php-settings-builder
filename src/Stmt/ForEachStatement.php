<?php

/*
 * This file is part of the PHP Settings Builder package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tinkersmith\SettingsBuilder\Php\Stmt;

use Tinkersmith\SettingsBuilder\Php\Dumper;
use Tinkersmith\SettingsBuilder\Php\Expr\Expression;
use Tinkersmith\SettingsBuilder\Php\Expr\ExpressionInterface;
use Tinkersmith\SettingsBuilder\Php\NamespacedInterface;

/**
 * Class for representing a "foreach" statement.
 *
 * The statement consists of a "foreach" value mapping express and the
 * statements to execute in the loop.
 */
class ForEachStatement extends StatementsBlock implements StatementInterface
{

    /**
     * The "foreach" iterable variable and "as" values.
     *
     * Expression should evaluate to: "$list as $key => $value".
     *
     * @todo improve the way we represent this to allow for more variations
     * of how this "foreach" expression is implemented.
     *
     * @var ExpressionInterface
     */
    protected ExpressionInterface $expr;

    /**
     * @param string|ExpressionInterface              $expr       The 'foreach' expression (e.g. "$list as $key => $value").
     * @param StatementInterface|StatementInterface[] $statements Either a single statement or an array of statements to
     *                                                            execute when the condition express evaluates to true.
     * @param string|null                             $comments   Comments for this statement.
     */
    public function __construct(string|ExpressionInterface $expr, array|StatementInterface $statements, public ?string $comments = null)
    {
        parent::__construct($statements, $comments);

        $this->expr = is_string($expr) ? new Expression($expr) : $expr;
    }

    /**
     * {@inheritdoc}
     */
    public function getNamespaces(): array
    {
        $namespaces = parent::getNamespaces();
        if ($this->expr instanceof NamespacedInterface) {
            $namespaces = array_merge($namespaces, $this->expr->getNamespaces());
        }

        return $namespaces;
    }

    /**
     * {@inheritdoc}
     */
    public function dump(Dumper $dumper, string $indent = ''): string
    {
        if (!$this->statements) {
            return '';
        }

        $output = $this->dumpComments($indent);
        $output .= "\n{$indent}foreach (".$dumper($this->expr).') {';
        $output .= $this->dumpStatements($this->statements, $dumper, $indent.$dumper->indent());

        return $output."\n{$indent}}\n";
    }
}
