<?php

/*
 * This file is part of the PHP Settings Buider package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tinkersmith\SettingsBuilder\Php;

use Drupal\node\Plugin\views\argument\Nid;
use Tinkersmith\SettingsBuilder\Php\Expr\AssignmentInterface;
use Tinkersmith\SettingsBuilder\Php\Stmt\StatementInterface;

/**
 * Class for SettingsBuilder to wrap items to write to the output destination.
 *
 * These object help track the flush and the written states of the item for
 * the SettingsBuilder so it can track if the item should be written or not.
 */
class BuildItem implements NamespacedInterface
{
    /**
     * Value to indicate if the SettingsBuilder has written this value yet.
     *
     * Used to indicate that a value should be written out when encountering
     * items that have the "flush" flag. WriteItems are all flushed at the
     * end of the SettingsBuilder output writing process.
     *
     * @var bool
     *
     * @see self::$flush
     */
    public bool $isWritten = false;

    /**
     * @param StatementInterface|AssignmentInterface $item  The item for the SettingsBuilder to build.
     * @param bool                                   $flush Should the settings builder flush previous items when this item is written.
     */
    public function __construct(public StatementInterface|AssignmentInterface $item, public bool $flush = false)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getNamespaces(): array
    {
        return $this->item instanceof NamespacedInterface
            ? $this->item->getNamespaces()
            : [];
    }


    /**
     * Write the code statement into an a string, and mark the item as written.
     *
     * @param SettingsBuilder $builder The PHP settings builder to generate the output with.
     * @param Dumper          $dumper  The PHP output dumper.
     * @param string          $indent  The current line indent for the output.
     *
     * @return string|null The outputs string for the item being wrapped.
     */
    public function dump(SettingsBuilder $builder, Dumper $dumper, string $indent = ''): ?string
    {
        $this->isWritten = true;

        return $dumper($this->item, $indent);
    }
}
