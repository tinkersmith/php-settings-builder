<?php

/*
 * This file is part of the PHP Settings Builder package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tinkersmith\SettingsBuilder\Php;

/**
 * Configured PHP style conventions to inform the writing of the settings.
 *
 * Depending on the PHP library or framework there are a lot of styling coding
 * conventions and when writing values to their settings files we'll want to
 * match those conventions as best we can.
 *
 * This class provides some values that can inform the PHP Dumper in what
 * conventions to follow.
 */
class PhpStyle
{

    /**
     * Used as the code indenting characters when indenting the PHP output.
     *
     * @var string
     */
    public string $indent = '  ';

    /**
     * Should booleans be output with all capital characters?
     *
     * For Symfony standard uses all "lowercase" ($capsBool = false) characters
     * and a standard like Drupal calls for all "uppercase" ($capsBool = true).
     *
     * @var bool
     */
    public bool $capsBool = true;

    /**
     * Should "null" be output with all capital characters?
     *
     * For Symfony standard uses all "lowercase" ($capsNull = false) characters
     * and a standard like Drupal calls for all "uppercase" ($capsNull = true).
     *
     * @var bool
     */
    public bool $capsNull = true;
}
