<?php

/*
 * This file is part of the PHP Settings Buider package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tinkersmith\SettingsBuilder\Php;

use Tinkersmith\SettingsBuilder\Php\Exception\UnclosedGeneratedBlockException;
use Tinkersmith\SettingsBuilder\Php\Exception\UnknownArrayKeyException;
use Tinkersmith\SettingsBuilder\Php\Expr\Assignment;
use Tinkersmith\SettingsBuilder\Php\Expr\AssignmentInterface;
use Tinkersmith\SettingsBuilder\Php\Stmt\StatementInterface;

/**
 * Helper class for writing and replacing values in a PHP settings file.
 *
 * This class is able to assist with inplace changes to variable assignments
 * and the addition of variable assignment statements for variables not found
 * by end of the file.
 *
 * @todo Improve the matching of nested array values, in terms of detection and
 * replacement. EX: Handling a value for $match['index1'] vs finding the
 * $match['index1']['nested'] value in the settings file. The assignment of
 * $match['index1'] must occur before the nested array value is assigned,
 * however, if $match['index1'] isn't be matched explicitly, it will be appended
 * to the end of the file, and unintentionally overwrite the nested values.
 *
 * There are likely to be other similiar situation where this helper is going
 * to incorrectly overwrite values.
 */
class SettingsBuilder
{

    /**
     * Regular expression for finding variable assignments in inline comments.
     *
     * Many PHP settings files come with common settings and defaults commented
     * out. This regex helps to identify those assignments, and allows the
     * PhpSettingsBuilder to place a compatible variable assignment here.
     *
     * The benefit is that it avoid confusion of having the setting created in
     * multiple places, and keeps the value assignment near the comments
     * describing that setting (if there was one).
     */
    protected const COMMENT_ASSIGN_REGEX = '/^#\s*(\$[\w_\[\]\.\'"]+)\s=/';

    /**
     * The pattern for the code generated blocks opening marker.
     *
     * @var string
     */
    public string $blockMarker = '@GeneratedBlock({{block_id}})';

    /**
     * The patterns for the code generated blocks closing marker.
     *
     * @var string
     */
    public string $blockEndMarker = '@endBlock({{block_id}})';

    /**
     * Should file include the "phpcs:ignoreFile" comment.
     *
     * Only applies when creating a new file.
     *
     * @var bool
     */
    protected bool $skipPhpcs = true;

    /**
     * List of assignments and code blocks to be written to the destination.
     *
     * This list maintains the list of items to write in the order they should
     * be expected to be written when flushed (items normally replace existing
     * locations if found before they could be flushed).
     *
     * @var BuildItem[]
     */
    protected array $buildList = [];

    /**
     * Value/setting assignments to apply with static::writeFile() file writes.
     *
     * Assignments are keyed by a the variable name. If the variable (left
     * handside of the assignment) is an array with a series of array keys, the
     * keys should be normalized using static::normalizeVarName() to ensure the
     * array key matching is consistent.
     *
     * @var BuildItem[]
     *
     * @see static::normalizeVarName()
     */
    protected array $assignments = [];

    /**
     * The generated code blocks keyed by the block identifier.
     *
     * Code blocks are delimited by the values of self::$blockMarker (start)
     * and self::$blockEndMarker (end) of the SettingsBuilder. These markers
     * appear in comments, and all the contents of the block are replaced by
     * the matching code block value in self::$codeBlocks with the matching
     * block ID. If no matching code block ID is found, the block is removed
     * from the source.
     *
     * Content in these code blocks are written at the very end when all the
     * unwritten BuildItems are flushed.
     *
     * @var BuildItem[]
     */
    protected array $codeBlocks = [];

    /**
     * @param Dumper $dumper The PHP settings output dumper object.
     */
    public function __construct(protected Dumper $dumper = new Dumper())
    {
    }

    /**
     * Set the PHP output coding style options for output.
     *
     * @param PhpStyle $style The PHP output style settings for code output.
     */
    public function setPhpStyle(PhpStyle $style): void
    {
        $this->dumper->style = $style;
    }

    /**
     * Ensure that variable names are created in a consistent way.
     *
     * The self::$assignments needs to be able to match keys consistently
     * so this method ensures that variable names are stored in a normalized
     * format.
     *
     * @param string  $varName The variable name.
     * @param mixed[] $keys    Any array indexes to apply to the variable name.
     *
     * @return string The normalized variable name.
     */
    public function normalizeVarName(string $varName, array $keys = []): string
    {
        if (!str_starts_with($varName, '$')) {
            $varName = '$'.$varName;
        }

        [$varName, $indexes] = explode('[', $varName, 2) + [null, null];
        if ($indexes) {
            $keys = array_merge(preg_split('/\]\[|\]/', $indexes, -1, PREG_SPLIT_NO_EMPTY), $keys);
        }

        if ($keys) {
            $cleanedKeys = array_map(function ($key) {
                return trim($key, '\'"');
            }, $keys);

            $varName .= '['.implode('][', $cleanedKeys).']';
        }

        return $varName;
    }

    /**
     * Add an value assignment expression to the settings file output.
     *
     * @param AssignmentInterface $assignment The assignment object to add to the settings file.
     * @param bool                $flush      When this assignment is written flush all build item up to this point.
     *
     * @return self
     */
    public function addAssignment(AssignmentInterface $assignment, bool $flush = false): self
    {
        $varName = $this->normalizeVarName($assignment->getVariableName());
        $item = new BuildAssignment($assignment, $flush);
        $this->assignments[$varName] = $item;
        $this->buildList[] = $item;

        return $this;
    }

    /**
     * Add an assignment for a value.
     *
     * Values can be a native PHP type or an ExpressionInterface object.
     *
     * @param string $varName The variable name to assign the value to.
     * @param mixed  $value   The value to use for the assignment expression.
     * @param bool   $flush   When this assignment is written flush all build item up to this point.
     *
     * @return self
     */
    public function assignValue(string $varName, mixed $value, bool $flush = false): self
    {
        return $this->addAssignment(new Assignment($varName, $value), $flush);
    }

    /**
     * Add value assignments for an array at a specified depth.
     *
     * A depth of zero means that the entire array should be written as the
     * value of $varName. Depths greater than zero will traverse to that array
     * depth before writing.
     *
     * Example depth of assignments:
     * <code>
     * // Depth of 2.
     * $varname[d1][d2] = {$value};
     *
     * // Depth of 1.
     * $varname[d1] = [
     *   d2 => {$value},
     * ];
     *
     * // Depth of 0.
     * $varname = [
     *   d1 => [
     *     d2 => {$value},
     *   ],
     * ];
     * </code>.
     *
     * @param string  $varName The variable name to create assignments for.
     * @param mixed[] $values  Array values to generate assignments for.
     * @param int     $depth   Indicates how many nesting levels to set the array value assignments.
     * @param bool    $flush   When this array is written flush all built items up to this point.
     *
     * @return self
     */
    public function assignArray(string $varName, array $values, int $depth = 0, bool $flush = false): self
    {
        if ($depth > 1) {
            foreach ($values as $key => $val) {
                if (\is_array($val)) {
                    $name = $varName.'['.($this->dumper)($key).']';
                    $this->assignArray($name, $val, $depth - 1, $flush);
                }
            }
        } elseif (1 === $depth) {
            foreach ($values as $key => $val) {
                $name = $varName.'['.($this->dumper)($key).']';
                $this->assignValue($name, $val, $flush);
            }
        } else {
            $this->assignValue($varName, $values, $flush);
        }

        return $this;
    }

    /**
     * Add a generated code block to be written to the destination file.
     *
     * @param string             $blockId The block identifier.
     * @param StatementInterface $block   The code block statements to write for this code block.
     * @param bool               $flush   This block flushes all unwritten items to this item in the $buildList.
     *
     * @return self
     */
    public function addGeneratedBlock(string $blockId, StatementInterface $block, bool $flush = true): self
    {
        $item = new BuildCodeBlock($blockId, $block, $flush);
        $this->buildList[] = $item;
        $this->codeBlocks[$blockId] = $item;

        return $this;
    }

    /**
     * Write a settings file applying the value assignments from this builder.
     *
     * @param string      $path       Path to the destination file. If no source file is provided, then this path is
     *                                also the source file. If file doesn't exist, a new file will be created.
     * @param string|null $src        The file to use as the source file. This file definition the default and initial
     *                                file contents. If absent the $path will be used as the source if it exists.
     * @param bool        $preferPath Prefer using the $path value as the source if the file already exists. When FALSE
     *                                attempt to use the file indicated by $src file if it exists.
     */
    public function writeFile(string $path, string $src = null, bool $preferPath = true): void
    {
        $src = ($preferPath && file_exists($path)) || empty($src) ? $path : $src;
        $content = file_exists($src) ? file_get_contents($src) : false;

        $buffer = '';
        $namespaces = new Namespaces();

        // Setup the dumper context.
        $this->dumper->namespaces = $namespaces;

        if (false === $content) {
            $buffer = "<?php\n\n";

            if ($this->skipPhpcs) {
                $buffer .= "// phpcs:ignoreFile\n\n";
            }

            $tokens = new TokenList([]);
        } else {
            $tokens = TokenList::fromText($content);

            // Collect all existing using statements so we can combine them with
            // the incoming declared using statements.
            foreach ($tokens as $token) {
                if (T_USE === $token[0]) {
                    $tokens->next();
                    $namespaces->addDeclared($tokens->findSemicolon());
                }
            }
        }

        // Add all the namespaces that from the build list.
        foreach ($this->buildList as $item) {
            if ($tmp = $item->getNamespaces()) {
                array_map($namespaces->getName(...), $tmp);
            }
        }

        $tokens->rewind();
        if ($namespaces->hasDeclarations()) {
            // First find where to place the using statements.
            while ($token = $tokens->current()) {
                $buffer .= $token[1];
                $tokens->next();

                // Skip whitespace, phpcs comments or file contents annotation.
                if (T_OPEN_TAG === $token[0]) {
                    while ($token = $tokens->current()) {
                        if (T_WHITESPACE === $token[0] || (T_COMMENT === $token[0] && preg_match("#^\\s*// phpcs:#i", $token[1]))) {
                            $buffer .= $token[1];
                            $tokens->next();
                            continue;
                        } elseif (T_DOC_COMMENT === $token[0] && preg_match("#(?:^|\n)\\s*\\*\\s*@file#i", $token[1])) {
                            $buffer .= $token[1];
                            $tokens->next();
                        }
                        break;
                    }
                    break;
                }
            }

            // Write out the sorted using declaration.
            $buffer = rtrim($buffer)."\n\n";
            foreach ($namespaces->getUseDeclarations() as $declaration) {
                $buffer .= $declaration.";\n";
            }
            $buffer .= "\n";
            $tokens->skipSpace();
        }

        // Search for value assignments to replace if the variable names
        // can be matched to assignments from the source file.
        while ($token = $tokens->current()) {
            [$type, $value] = $token;
            $tokens->next();

            // @todo Need to track situations where a value assignment might
            // not be within parentheses (if, for, or while statements) or
            // as part of a tertiary expression. The assignment doesn't
            // always terminate with a semicolon in these cases as we are
            // relying on at the end of this "if" statement.
            // Need improve the detection of the end of an assignment.
            if (T_VARIABLE === $type) {
                $varName = $value;
                $tmp = $value.$tokens->skipSpace();

                try {
                    [$type, $value] = $tokens->current() ?? [null, null];

                    // If variable has array indexes parse and apply those
                    // for name matching and writing the assignment output.
                    if ('[' === $value) {
                        $keys = $tokens->parseArrayKeys();
                        $varName = $this->normalizeVarName($varName, $keys);
                        $tmp .= '['.implode('][', $keys).']'.$tokens->skipSpace();
                        [$type, $value] = $tokens->current() ?? [null, null];
                    }

                    // If this is an assignment and the variable name is
                    // a settings that this builder wants to replace, write
                    // out the replacement value.
                    if ('=' === $value && !empty($this->assignments[$varName])) {
                        $indent = '';
                        $buffer = preg_replace_callback("/(?<=\n)[ \t]+$/", function ($m) use (&$indent) {
                            $indent = $m[0];

                            return '';
                        }, $buffer, 1);

                        $buffer .= $this->outputBuildItem($this->assignments[$varName], $indent);
                        $tokens->findSemicolon();
                        continue;
                    }
                } catch (UnknownArrayKeyException $e) {
                    // Found an array key that can't be resolved for a
                    // value substitution, fall through. The token list
                    // position will be reset and can be written to the
                    // buffer.
                }

                $buffer .= $tmp.$tokens->findSemicolon();
            } elseif (T_USE === $type) {
                // Skip additional use declarations as these have been handled
                // already by the namespaces objects.
                $tokens->findSemicolon();
                $tokens->skipSpace();
            } elseif (T_COMMENT === $type && preg_match(self::COMMENT_ASSIGN_REGEX, $value, $matches)) {
                $varName = $this->normalizeVarName($matches[1]);

                if (!empty($this->assignments[$varName])) {
                    $buffer .= $this->outputBuildItem($this->assignments[$varName]);
                } else {
                    $buffer .= $value;
                }
            } elseif (in_array($type, [T_COMMENT, T_DOC_COMMENT])) {
                if ($blockId = $this->getGeneratedBlockId($value)) {
                    $this->findGeneratedBlockEnd($blockId, $tokens);

                    // See if we can find the generated block segment to replace this with.
                    if (!empty($this->codeBlocks[$blockId])) {
                        $buffer .= $this->outputBuildItem($this->codeBlocks[$blockId]);
                    }
                } else {
                    // @todo parse commments for utilized using namespaces.
                    $buffer .= $value;
                }
            } else {
                $buffer .= $value;
            }
        }

        // Write any remaining build items.
        $buffer .= $this->flushBuildList();
        file_put_contents($path, $buffer);
    }

    /**
     * Render the build item.
     *
     * Formats and renders the item into a string, and if the "flush" flag is
     * set for the item, then also render all unwritten items from the
     * $buildList until this buildItem is found.
     *
     * @param BuildItem $item   The item to build.
     * @param string    $indent The amount of space to indent lines.
     *
     * @return string The rendered output of the built items.
     */
    protected function outputBuildItem(BuildItem $item, string $indent = ''): string
    {
        $output = '';
        if ($item->flush) {
            $output .= $this->flushBuildList($item);
        }

        return $output.$item->dump($this, $this->dumper, $indent);
    }

    /**
     * Build all items which haven't been written yet.
     *
     * If a $find parameter is provided, the flushing / rendering stops when the
     * item is found in the $buildList.
     *
     * @param BuildItem|null $find The build item to flush all build items until. If NULL all items will be built.
     *
     * @return string The rendered output of all the flushed items.
     */
    protected function flushBuildList(?BuildItem $find = null): string
    {
        $output = '';
        foreach ($this->buildList as $item) {
            if ($item === $find) {
                break;
            }

            if (!$item->isWritten) {
                $tmp = $item->dump($this, $this->dumper);
                $output .= empty($tmp) ? '' : "{$tmp}\n";
            }
        }

        return $output;
    }

    /**
     * Check if a comment block contains a generated code block opening marker.
     *
     * @param string $comment Comment value to check for a generated code block opening tag.
     *
     * @return string|null If the comment is a generated code block opening marker, the ID of the code block. Otherwise
     *                     return NULL if this comment does not contain a code block starting marker.
     */
    protected function getGeneratedBlockId(string $comment): ?string
    {
        $regex = str_replace('\{\{block_id\}\}', '([\w_]+)', preg_quote($this->blockMarker, '/'));
        if (preg_match("/\n\s*(?:(?:\*|\/\/|#)\s*)?{$regex}/im", $comment, $matches)) {
            return $matches[1];
        }

        return null;
    }

    /**
     * Find the closing generated code block comment in the PHP tokens list.
     *
     * This method advances the position of the token lists to the end of the
     * code block ending marker comments. This allows token parsing or output
     * to continue after this generated block contents (write new block contents
     * after this to replace the block).
     *
     * @param String    $blockId The code block identifier.
     * @param TokenList $tokens  The PHP tokens to search for the closing block.
     */
    protected function findGeneratedBlockEnd(string $blockId, TokenList $tokens): void
    {
        $marker = str_replace('{{block_id}}', $blockId, $this->blockEndMarker);
        $regex = "/\n\s*(?:(?:\*|\/\/|#)\s*)?".preg_quote($marker, '/').'/im';

        while ($token = $tokens->current()) {
            [$type, $value] = $token;
            $tokens->next();

            // Try to find the ending generated code block marker.
            if (in_array($type, [T_COMMENT, T_DOC_COMMENT]) && preg_match($regex, $value)) {
                return;
            }
        }

        throw new UnclosedGeneratedBlockException($blockId);
    }
}
