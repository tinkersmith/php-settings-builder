<?php

/*
 * This file is part of the PHP Settings Builder package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tinkersmith\SettingsBuilder\Php;

use Tinkersmith\SettingsBuilder\Php\Exception\UnknownArrayKeyException;

/**
 * Manages a list of PHP script token.
 *
 * Allows the PHP script tokenized values to be traversed, and has some
 * convience methods to skip white space, or parse array keys.
 *
 * @implements \Iterator<int, mixed[]>
 */
final class TokenList implements \Iterator
{

    /**
     * Initiates a PhpTokenList object from PHP script content.
     *
     * @param string $text The PHP code content to tokenize.
     *
     * @return self
     */
    public static function fromText(string $text): self
    {
        return new static(token_get_all($text));
    }

    /**
     * The current token position.
     *
     * @var int
     */
    protected int $idx;

    /**
     * @param mixed[] $tokens The array of tokenized PHP script.
     *
     * @see token_get_all()
     */
    public function __construct(protected array $tokens)
    {
        $this->idx = 0;
    }

    /**
     * @return bool TRUE if the current token position is valid, FALSE if we've traversed to the end of the tokens.
     */
    public function valid(): bool
    {
        return (bool) isset($this->tokens[$this->idx]);
    }

    /**
     * Reset the internal token position pointer, and go back to the start.
     */
    public function rewind(): void
    {
        $this->idx = 0;
    }

    /**
     * @return int Get the current internal token position.
     */
    public function key(): int
    {
        return $this->idx;
    }

    /**
     * Get the value of the token at the current token list position.
     *
     * @return mixed[]|null If the current token position is valid, return an array with the current token
     *                                 type, and the current token value. NULL is returned if no token is available at
     *                                 the current token position.
     */
    public function current(): ?array
    {
        if ($this->valid()) {
            $value = $this->tokens[$this->idx];

            return is_array($value) ? $value : [-1, $value];
        }

        return null;
    }

    /**
     * Move the token position to the next position, and return the token value.
     *
     * @return void The current token value after moving to the next position or NULL if there are
     *                                 no more tokens at the new position.
     */
    public function next(): void
    {
        ++$this->idx;
    }

    /**
     * Move the token position to the next non-space token.
     *
     * @return string The values of the space tokens skipped. This is provided incase the caller wants to output these.
     */
    public function skipSpace(): string
    {
        $sp = '';

        while ($token = $this->current()) {
            if (T_WHITESPACE !== $token[0]) {
                return $sp;
            }
            $sp .= $token[1];
            $this->next();
        }

        return $sp;
    }

    /**
     * Move the token position to the next non-space and non-comment token.
     *
     * @return string The values of the tokens skipped. This is provided incase the caller wants to output these.
     */
    public function skipSpaceOrComment(): string
    {
        $sp = '';

        while ($token = $this->current()) {
            if (T_WHITESPACE !== $token[0] && T_COMMENT !== $token[0] && T_DOC_COMMENT !== $token[0]) {
                return $sp;
            }
            $sp .= $token[1];
            $this->next();
        }

        return $sp;
    }

    /**
     * Capture the array key values for an array variable.
     *
     * This method only captures keys which the value can be resolved. If the
     * array keys are a variable, an expression or an array append ('[]'), then
     * the keys are non-deterministic and this method doesn't know their
     * intended values and will throw an exception to indicate that.
     *
     * @return mixed[] The array key values.
     *
     * @throws \InvalidArgumentException If the tokens do not appear to be an array index (did not start with '[').
     * @throws UnknownArrayKeyException  If there are array key values that are non-deterministic.
     */
    public function parseArrayKeys(): array
    {
        $keys = [];
        [, $value] = $this->current();

        if ('[' !== $value) {
            throw new \InvalidArgumentException();
        }

        $start = $this->idx;
        try {
            $this->next();
            while ($this->current()) {
                $this->skipSpace();
                [$type, $value] = $this->current();

                if (in_array($type, [T_LNUMBER, T_DNUMBER, T_CONSTANT_ENCAPSED_STRING])) {
                    $keys[] = $value;
                    $this->next();
                    $this->consumeSequence([']'], true);

                    // Is there a sub-array index to check for array keys?
                    if (!$this->isNext('[')) {
                        break;
                    }
                } else {
                    // Array key can be invalid, be an expression, a variable
                    // or just be empty (append to array). These all result in
                    // array key values that we can't set values for, since we
                    // cannot tell what these will evaluate to.
                    throw new UnknownArrayKeyException();
                }
                $this->next();
            };
        } catch (\Throwable $e) {
            $this->idx = $start;

            if ($e instanceof \InvalidArgumentException) {
                // If thrown from static::consumeSequence() it means we did not
                // encounter a closing square bracket, which means this key
                // could be an expression, and has an unknown key value.
                throw new UnknownArrayKeyException($e->getMessage(), 0, $e);
            }
            throw $e;
        }

        return $keys;
    }

    /**
     * Checks if the next token matches an expected value or type.
     *
     * Depending on the value of $consumeMatch, the position of the token list
     * will be at the start of the match, or end after the match. If no match
     * was found, the position of the token list will not change.
     *
     * @param string|int $match        If a string the value to match, if an integer, then match the token type.
     * @param bool       $allowSpace   Skip over any spaces before attempting a match.
     * @param bool       $consumeMatch If the matched, should the internal token point to after the matched value? TRUE
     *                                 if the internal position should be moved after. If no match or this is parameter
     *                                 is FALSE the token position will remain unchamged.
     *
     * @return bool TRUE if the expected $match was found, otherwise FALSE.
     */
    public function isNext(string|int $match, bool $allowSpace = true, bool $consumeMatch = true): bool
    {
        $start = $this->idx;

        if ($allowSpace) {
            $this->skipSpace();
        }

        $isToken = !is_string($match);
        [$type, $value] = $this->current() ?? [null, null];

        if ((!$isToken && $value === $match) || ($isToken && $type === $match)) {
            // If not set to consume, then only peek ahead, and then go back to
            // the initial token position when this method peeked ahead.
            if (!$consumeMatch) {
                $this->idx = $start;
            }

            return true;
        }

        // We did not find the expected token, rewind back to the start index.
        $this->idx = $start;

        return false;
    }

    /**
     * Find the next semicolon from the current position.
     *
     * @return string The values of the traversed tokens while finding the semicolon.
     */
    public function findSemicolon(): string
    {
        $buffer = '';
        while ($curr = $this->current()) {
            $buffer .= $curr[1];

            if ($curr[1] === ';') {
                $this->next();
                break;
            }

            $this->next();
        }

        return $buffer;
    }

    /**
     * Find a match to the provided $sequence values.
     *
     * The sequence is expected to be the next set of values, and will error
     * if the next tokens do not match.
     *
     * @param mixed[]|string|int $sequence   A sequence of token types or token values to attempt to match.
     * @param bool               $allowSpace If TRUE, skip over any whitespace.
     *
     * @return string The traversed token values, including the consumed sequence.
     *
     * @throws \InvalidArgumentException When the next set of tokens do not match the expected sequence.
     */
    protected function consumeSequence(array|string|int $sequence, bool $allowSpace = true): string
    {
        $buffer = '';
        $start = $this->idx;
        $sequence = is_array($sequence) ? $sequence : [$sequence];

        foreach ($sequence as $needle) {
            if ($allowSpace) {
                $buffer .= $this->skipSpace();
            }

            $isToken = !is_string($needle);
            [$type, $value] = $this->current() ?? [null, null];

            if ((!$isToken && $value !== $needle) || ($isToken && $type !== $needle)) {
                $this->idx = $start;
                throw new \InvalidArgumentException(sprintf('Unexpected characters encountered during parsing: %s', $value));
            }

            $this->next();
            $buffer .= $value;
        }

        return $buffer;
    }
}
