<?php

/*
 * This file is part of the PHP Settings Builder package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tinkersmith\SettingsBuilder\Php;

/**
 * Maintains class namespaces for a context (SettingsBuilder).
 *
 * The namespaces context is important to inform SettingsBuilder instances of
 * what namespaces to include, and maintain and manage the class aliases in
 * the generated files.
 */
class Namespaces
{

    /**
     * Declared use that have been encountered to this point.
     *
     * The array key is the fully namespaced class name and the values are the
     * class name aliases to use for them.
     *
     * @var mixed[]
     */
    protected array $used = [];

    /**
     * List of all class aliases, or reference names.
     *
     * This list is used to keep track of which class aliases have already been
     * used and helps to indicate when an alternative alias needs to be created.
     *
     * @var string[]
     */
    protected array $names = [];

    /**
     * Method to determine if any namespaces are available in this context.
     *
     * @return bool Does this instance have any namespace declarations available?
     */
    public function hasDeclarations(): bool
    {
        return !empty($this->used);
    }

    /**
     * Get a list of declared class namespaces formatted as "use" statements.
     *
     * @return mixed[] An array of all the class namespaces formatted as "use" statements.
     */
    public function getUseDeclarations(): array
    {
        ksort($this->used, SORT_STRING);

        $declarations = [];
        foreach ($this->used as $key => $def) {
            $declarations[] = "use {$key}".($def['as'] ? " as $def[name]" : '');
        }

        return $declarations;
    }

    /**
     * Add a declared class namespace definition to the namespaces context.
     *
     * The declared class namespaces are the "use" statements that appear at the
     * beginning of PHP classes. These provide the fully namespaced names of the
     * class definitions and can include an "as" directive to create the class
     * alias.
     *
     * The alias is recorded and used for future ::getName() calls.
     *
     * @param string $declared The class namespace definition to add.
     */
    public function addDeclared(string $declared): void
    {
        if (preg_match('/([\w_\\\\]+)(?:\s+as\s+([\w_]+))?\s*;?$/i', trim($declared), $matches)) {
            $as = true;

            if (empty($matches[2])) {
                $pos = strrpos($matches[1], '\\');
                $matches[2] = (false === $pos) ? $matches[1] : substr($matches[1], $pos + 1);
                $as = false;
            }

            // Record the declaration information and class names in use.
            $this->names[$matches[2]] = true;
            $this->used[$matches[1]] = [
                'name' => $matches[2],
                'as' => $as,
            ];
        }
    }

    /**
     * Get the class name to use based on the current namespace contexts.
     *
     * Reviews the current namespaces made available in the current context
     * with declared class namespaces ("use" statements) and returns the name
     * the class should be referenced as.
     *
     * @param string $classname The fully namespaced classname to get the name to use in the current context.
     * @param bool   $addClass  Should the class be added to the current namespace definition if it does not already
     *                          exist in this context. Note that this should only be used if these namespaces will be
     *                          added to any file output before this class is used.
     *
     * @return string The reference for the class to use in this context with the currently known namespaces.
     */
    public function getName(string $classname, bool $addClass = true): string
    {
        // Normalize the namespace and exclude the leading backspace.
        $classname = ltrim($classname, '\\');
        if (!empty($this->used[$classname])) {
            return $this->used[$classname]['name'];
        }

        // Store this full namespaced class with to be found quickly for next
        // usage, and also so it can be added to the use declarations.
        if ($addClass) {
            $parts = explode('\\', $classname);
            $name = array_pop($parts);
            $as = false;

            while (isset($this->names[$name])) {
                $as = true;

                if (!$parts) {
                    // Should be extremely rare but we were not able to create
                    // a alias for this class using the namespace parts, so
                    // use the full namespaced class.
                    return '\\'.$classname;
                }

                // Append the next portion of namespace to try to make this unique.
                $name = array_pop($parts).$name;
            }

            $this->used[$classname] = [
                'name' => $name,
                'as' => $as,
            ];
        } else {
            $name = '\\'.$classname;
        }

        return $name;
    }
}
