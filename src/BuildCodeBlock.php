<?php

/*
 * This file is part of the PHP Settings Buider package.
 *
 * (c) Liem Khuu <liem@tinkersmith.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tinkersmith\SettingsBuilder\Php;

use Tinkersmith\SettingsBuilder\Php\Expr\AssignmentInterface;
use Tinkersmith\SettingsBuilder\Php\Stmt\StatementInterface;

/**
 * Class for SettingsBuilder to represent a generated code block.
 */
class BuildCodeBlock extends BuildItem
{

    /**
     * @param string             $id    The generated code block ID.
     * @param StatementInterface $item  The code block statements and comments.
     * @param bool               $flush Should the settings builder flush previous items when this item is written.
     */
    public function __construct(protected string $id, public StatementInterface|AssignmentInterface $item, public bool $flush = false)
    {
        if (!$item instanceof StatementInterface) {
            throw new \InvalidArgumentException('The build item for the BuildCodeBlock class must be a Statement type.');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function dump(SettingsBuilder $builder, Dumper $dumper, string $indent = ''): ?string
    {
        $this->isWritten = true;

        // No need to output a code block that is empty.
        if ($this->item->isEmpty()) {
            return null;
        }
        $startMarker = str_replace('{{block_id}}', $this->id, $builder->blockMarker);
        $endMarker = str_replace('{{block_id}}', $this->id, $builder->blockEndMarker);

        if ($comments = $this->item->comments ?? '') {
            // Remove comment characters so we can replace them with a standard
            // block section markers and enforce a consistent format.
            $comments = preg_replace(
                ['#(^|\n)(/\*\*?.*\n|\s*\*/)#m', '#\n\s*(//|\#|\*)#m'],
                ['', "\n *"],
                $comments
            )."\n";
        } else {
            $comments = <<<EOT
                 * This code was generated and any changes may be lost on next file update.

                EOT;
        }

        return "/**\n$comments *\n * {$startMarker}\n */\n"
            .$dumper($this->item, $indent)
            ."\n\n/**\n * {$endMarker}\n */";
    }
}
